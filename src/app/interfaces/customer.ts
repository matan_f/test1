export interface Customer {
    id?:string,
    name: string,
    years: number,
    income:number,
    saved?:Boolean;
    result?:string;   
}
