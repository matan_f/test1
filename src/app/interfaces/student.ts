export interface Student {
    id?:string,
    mathematics: number,
    psychometric:number,
    payed?:boolean;
    saved?:boolean;
    result?:string;
    drop:string;
}
