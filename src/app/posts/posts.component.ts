import { AuthService } from './../auth.service';
import { AppRoutingModule } from './../app-routing.module';
import { PostsService } from './../posts.service';
import { Component, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;

  posts$:Post[]=[];
  comments$: Comment[] =[];
  postId:number;
  postComment;
  userId:string;

  savePost(title:string){
    
    this.postsService.savePost(this.userId,title);
    console.log("Post saved");

  }
  
  
  // authService: any;

 

  // getComments(id:number){
  //   this.postComment=this.postsService.getComments(id)
  //   console.log(id);
  //   this.router.navigate(this.postComment)


  // }
 

  
  // getComments(postId):Observable<Comment>{

  //   return this.http.get<Comment>(`http://jsonplaceholder.typicode.com/posts/${postId}/comments`); 
  // }

  constructor(private postsService:PostsService,private http: HttpClient, public authService:AuthService,public router:Router) { }

  ngOnInit(): void {
    
  
    
    // savePost(title,body){
    //   this.postsService.savePost(this.userId,title,body)

    // }
    this.postsService.getComments().subscribe(data => this.comments$ = data)
    // this.posts$ = this.postsService.getPosts(); 
    this.postsService.getPosts().subscribe(data =>this.posts$ = data);
    
    
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
      })

      
      // savePost(userId:string,id:number,title: string,body: string){
      //   this.postsservice.savePost(this.userID,title,body)
      //   console.log("Post saved");
      //   this.router.navigate(['/savedpost']);  
      
      // }





    // this.comments$=this.postsService.getComments()



    // this.postsService.getPosts().subscribe(
    //   post => {
    //     this.postId = post.id;
    //     console.log(this.postId); 
        
    // this.postId = post.id;
    // // this.authService.getUser().subscribe(
    // //   user => {
    // //     this.userId = user.uid;
    // //     console.log(this.userId);


    // // this.comments$=this.postsService.getComments()   
    
    //   }
    // );
    
  

    
  }
}
