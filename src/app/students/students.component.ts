import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Student } from './../interfaces/student';
import { Component, OnInit } from '@angular/core';
import { StudentsService } from '../students.service';
import { AuthService } from '../auth.service';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  student:Student;
  posts$:Observable<any>;
  userId:string; 
  editstate = [];
  addStudentFormOPen = false;
  panelOpenState = false;

  constructor(private studentsService:StudentsService, public authService:AuthService,private predictionService:PredictionService,private postsService:PostsService) { }

  updateResult(index){
    this.student[index].saved = true; 
    this.studentsService.updateResult(this.userId,this.student.id,this.student[index].result);
  }
  

  // add(student:Student){
  //   // console.log(student)
  //   this.studentsService.addStudents(this.userId,this.student.mathematics,this.student.psychometric,this.student.payed); 
  // }
  add(Student:Student){
    this.studentsService.addStudents(this.userId,Student.mathematics,Student.psychometric,Student.payed,Student.drop); 
  }

  // predict(){
  //   this.customers.result = 'Will difault';
  //   this.predictionService.predict(this.student.mathematics, this.customers.psychometric).subscribe(
  //     res => {console.log(res);
  //       if(res > 0.5){
  //         var result = 'Will pay';
  //       } else {
  //         var result = 'Will default'
  //       }
  //       this.customers[index].result = result}
  //   );  
  // }
  
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.posts$ = this.postsService.getUserPost(this.userId);

      })

    
  }

}
