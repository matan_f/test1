import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  // posts$:Observable<Post>;
  Numberoflikes:number;

  userCollection:AngularFirestoreCollection;  
  

  private URL = "https://jsonplaceholder.typicode.com/posts/";
  private CURL ="http://jsonplaceholder.typicode.com/comments"
  constructor(private http: HttpClient,private db:AngularFirestore ) { }

  getComments():Observable<Comment[]>{

    return this.http.get<Comment[]>(this.CURL); 
  }

  getPosts():Observable<Post[]>{
    return this.http.get<Post[]>(this.URL); 
  }

  savePost(userId:string,title: string){
    const post = {title:title }; 
    this.db.collection('users').doc(userId).collection('posts').doc().set(post);
    // const res =  this.userCollection.doc(userId).collection('posts').doc().set(post);

    //  this.userCollection.doc(userId).collection('posts').add(post);
    // this.db.doc(`users/${userId}/${col}`).add(post)
  }

  getUserPost(userId:string):Observable<any[]>{
    // return this.db.collection('books').valueChanges({idField:'id'});
    console.log("in getUserPost ")
    this.userCollection = this.db.collection(`users/${userId}/posts`);
     return this.userCollection.snapshotChanges().pipe(
       map(
         collection => collection.map(
           document => {
             const data = document.payload.doc.data();
             data.id = document.payload.doc.id;
             return data;
           }
         )
   
       )
     )
   
   }
   
   deletepost(id:string , userId:string){
   
    this.db.doc(`users/${userId}/posts/${id}`).delete();
   }

   likepost(id:string , userId:string, likes:number){

    this.Numberoflikes = (likes+1);
    this.db.doc(`users/${userId}/posts/${id}`).update({
      likes: this.Numberoflikes,

    })
   }
  



}
