import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  customerCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getCustomers(userId){
    this.customerCollection = this.db.collection(`users/${userId}/customers`, 
    ref => ref.orderBy('name', 'asc')); 
    return this.customerCollection.snapshotChanges()      
  } 


  deleteCustomer(Userid:string, id:string){
    this.db.doc(`users/${Userid}/customers/${id}`).delete(); 
  } 

  addCustomer(userId:string,name:string,years:number,income:number){
    const customer = {name:name, years:years, income:income}; 
    this.userCollection.doc(userId).collection('customers').add(customer);
  }

  updateCustomer(userId:string,id:string,name:string,years:number,income:number){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        name:name,
        years:years,
        income:income
      }
    )
  }

  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      })
    }

  constructor(private db:AngularFirestore) { }
}
