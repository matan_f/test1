import { StudentsService } from './../students.service';
import { Student } from './../interfaces/student';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'studentform',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  @Input() payed:boolean;  
  @Input() mathematics:number; 
  @Input() psychometric:number; 
  @Input() id:string; 
  @Input() formType:string;
  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();

  show:boolean=false;
  math:number;
  psy:number;
  bpayed:boolean;
  spayed:string;
  result:number;
  bresult:boolean;
  drop:string;

  onSubmit(mathematics,psychometric ,payed){
    if (payed == true){
      this.spayed="true"

    }else{
      this.spayed="false"

    }
    console.log(mathematics,psychometric,this.bpayed,this.spayed)
    this.studentsService.pre(mathematics, psychometric, this.spayed).subscribe(
      res => {
        console.log(res);
        this.result = res;
        if(this.result>0.5) {
          this.bresult=true;
        }else{
          this.bresult=false;
        }
        this.show=true;      
      }
    )
  }

  Close(){
    this.show=!this.show;
  }
  
   
  updateParent(){
    let student:Student = {id:this.id, mathematics:this.mathematics, psychometric:this.psychometric,  payed:this.payed, drop:this.drop};
    console.log(this.mathematics,this.payed, this.psychometric)
    this.update.emit(student); 
    if(this.formType == "Add student"){
      this.payed  = null;
      this.mathematics = null; 
      this.psychometric = null; 
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  constructor(private studentsService:StudentsService) { }

  ngOnInit(): void {
  }

}
