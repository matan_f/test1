import { SavedstudentsComponent } from './savedstudents/savedstudents.component';
import { SavedpostsComponent } from './savedposts/savedposts.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ByeComponent } from './bye/bye.component';
import { PostsComponent } from './posts/posts.component';
import { CustomersComponent } from './customers/customers.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';
import { CityFormComponent } from './city-form/city-form.component';
import { StudentsComponent } from './students/students.component';


const routes: Routes = [
 
  { path: 'welcome', component: WelcomeComponent },

  { path: 'login', component: LoginComponent},  
  { path: 'signup', component: SignUpComponent},
  { path: 'bye', component: ByeComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'savedposts', component: SavedpostsComponent },
  { path: 'temperatures/:city', component: TemperaturesComponent },
  { path: 'city', component: CityFormComponent }, 
  { path: 'students', component: StudentsComponent },
  { path: 'savedstudents', component: SavedstudentsComponent }





];


@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes),
    CommonModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
