import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  getUserStudent(userId:string):Observable<any[]>{
    // return this.db.collection('books').valueChanges({idField:'id'});
    console.log("getUserStudent")
    this.userCollection = this.db.collection(`users/${userId}/students`);
     return this.userCollection.snapshotChanges().pipe(
       map(
         collection => collection.map(
           document => {
             const data = document.payload.doc.data();
             data.id = document.payload.doc.id;
             return data;
           }
         )
   
       )
     )
   
   }

  studentsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');


  public getStudents(userId){
    this.studentsCollection = this.db.collection(`users/${userId}/students`, 
    ref => ref.orderBy('name', 'asc')); 
    return this.studentsCollection.snapshotChanges()      
  } 


  deleteStudents(Userid:string, id:string){
    this.db.doc(`users/${Userid}/students/${id}`).delete(); 
  } 

  addStudents(userId:string,mathematics:number,psychometric:number,payed:boolean,drop:string){
    const student = {mathematics:mathematics, psychometric:psychometric, payed:payed, drop:drop}; 
    this.userCollection.doc(userId).collection('students').add(student);
  }

  updateStudents(userId:string,id:string,mathematics:number,psychometric:number,payed:boolean){
    this.db.doc(`users/${userId}/student/${id}`).update(
      {
        mathematics:mathematics,
        psychometric:psychometric,
        payed:payed
      }
    )
  }

  updateResult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/student/${id}`).update(
      {
        result:result
      })
    }

    private url = "https://e172soa33h.execute-api.us-east-1.amazonaws.com/ftest";
    pre(math:number, psy:number, pay:string):Observable<any>{
      let json = {
        "data": 
          {
            "mathematics": math,
            "psychometric": psy,
            "payed":pay
          }
      }
      let body  = JSON.stringify(json);
      console.log("works");
      console.log(json);
            return this.http.post<any>(this.url,body).pipe(
        map(res => {
          console.log("in the map"); 
          console.log(res);
          console.log(res.body);
          return res.body;       
        })
      );      
    }  

  constructor(private db:AngularFirestore, private http:HttpClient) { }
}
