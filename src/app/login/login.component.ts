import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  error:string = 'no error'; 
  hasError:Boolean = false; 

  onSubmit(){
    this.AuthService.login(this.email,this.password)
    .then(res => 
      {
        console.log('Succesful sign up',res)
        this.router.navigate(['/welcome']);
      }
    ).catch(
      err => {
        console.log('in the catch')
        this.hasError = true;
        this.error = err.message;  
        console.log(this.error);
      }
    );
    
  }


  constructor(private AuthService:AuthService,
    private router:Router) { }
  ngOnInit(): void {
  }

}
