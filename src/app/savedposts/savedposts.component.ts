import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { PostsService } from '../posts.service';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  constructor(private postsService:PostsService, public authService:AuthService, public router:Router) { }

  poosts:any;
  posts$:Observable<any>;
  userId:string;

  deletepost(id:string){
    this.postsService.deletepost(id , this.userId);
  } 

  likepost(id:string, likes:number){

    console.log(likes)
    if (isNaN(likes)){
      likes=0
      console.log(likes)
    }
    this.postsService.likepost( id , this.userId, likes);
  }

  ngOnInit(): void {

    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.postsService.getUserPost(this.userId);
      }
    )

    

  }

}
