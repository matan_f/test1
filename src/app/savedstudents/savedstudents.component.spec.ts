import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedstudentsComponent } from './savedstudents.component';

describe('SavedstudentsComponent', () => {
  let component: SavedstudentsComponent;
  let fixture: ComponentFixture<SavedstudentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SavedstudentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedstudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
