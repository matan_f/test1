import { StudentsService } from './../students.service';
import { PostsService } from './../posts.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-savedstudents',
  templateUrl: './savedstudents.component.html',
  styleUrls: ['./savedstudents.component.css']
})
export class SavedstudentsComponent implements OnInit {
  userId:string;
  students$:Observable<any>;
  constructor(private authService:AuthService,private StudentsService:StudentsService) { }

  ngOnInit(): void {

    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        this.students$ = this.StudentsService.getUserStudent(this.userId);
      }
    )

    

  }

}
