import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  email:string;
  password:string;
  error:string = 'no error'; 
  hasError:Boolean = false; 

  onSubmit(){
    this.authService.SignUp(this.email,this.password)
    .then(res => 
      {
        console.log('Succesful sign up',res)
        this.router.navigate(['/welcome']);
      }
    ).catch(
      err => {
        console.log('in the catch')
        this.hasError = true;
        this.error = err.message;  
        console.log(this.error);
      }
    );
  }

  constructor(private authService:AuthService,
    private router:Router) { }
 


  ngOnInit(): void {
  }

}
