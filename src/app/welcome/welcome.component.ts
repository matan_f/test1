import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { User } from '../interfaces/user';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  user:Observable<User | null>;
  userId:string;
  userEmail:string;

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        
    this.userId = user.uid;
    this.userEmail=user.email;
      }
    );
        
  }

}
