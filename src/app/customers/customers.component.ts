import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  customers$; 
  customers:Customer[];
  userId:string; 
  editstate = [];
  addCustomerFormOPen = false;
  panelOpenState = false;

  constructor(private customersService:CustomersService, public authService:AuthService,private predictionService:PredictionService) { }

  updateResult(index){
    this.customers[index].saved = true; 
    this.customersService.updateResult(this.userId,this.customers[index].id,this.customers[index].result);
  }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id); 
  }

  
  update(customer:Customer){
    this.customersService.updateCustomer(this.userId,customer.id ,customer.name, customer.years,customer.income);
  }

  add(customer:Customer){
    this.customersService.addCustomer(this.userId,customer.name,customer.years,customer.income); 
  }

  predict(index){
    this.customers[index].result = 'Will difault';
    this.predictionService.predict(this.customers[index].years, this.customers[index].income).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will default'
        }
        this.customers[index].result = result}
    );  
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customersService.getCustomers(this.userId); 
        
        this.customers$.subscribe(
          docs =>{
            console.log('init worked');
            // this.lastDocumentArrived = docs[docs.length-1].payload.doc;
            // this.firstDocumentArrived = docs[0].payload.doc;
            // this.push_prev_startAt(this.firstDocumentArrived);             
            this.customers = [];
            for(let document of docs){
              const customer:Customer = document.payload.doc.data();
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
            }
          }
        ) 
      }
    )

  }

}
